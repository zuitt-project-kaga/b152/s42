console.log("Hello World");

console.log(document);

let firstNameLabel = document.querySelector("#label-first-name");

console.log(firstNameLabel);

let lastNameLabel = document.querySelector("#label-last-name");

console.log(lastNameLabel);

console.log(firstNameLabel.innerHTML);

firstNameLabel.innerHTML = "I like New York City.";

lastNameLabel.innerHTML = "My Favorite Food is Sisig";

let city = "Tokyo";
if (city === "New York") {
  firstNameLabel.innerHTML = `I like New York.`;
} else {
  firstNameLabel.innerHTML = `I don't like New York. I like ${city} city.`;
}

let toggle = true;
firstNameLabel.addEventListener("click", () => {
  toggle = !toggle;
  toggle
    ? (firstNameLabel.innerHTML = "I've been cliked. Send help!")
    : (firstNameLabel.innerHTML = "No need help!");
  firstNameLabel.style.color = "red";
  firstNameLabel.style.fontSize = "10vh";
});

lastNameLabel.addEventListener("click", () => {
  if (lastNameLabel.style.color === "blue") {
    lastNameLabel.style.color = "black";
    lastNameLabel.style.fontSize = "16px";
  } else {
    lastNameLabel.style.color = "blue";
    lastNameLabel.style.fontSize = "5vh";
  }
});

let inputFirstName = document.querySelector("#txt-first-name");
console.log(inputFirstName.value);
inputFirstName.addEventListener("keyup", () => {
  console.log(inputFirstName.value);
});

let fullNameInput = document.querySelector("#full-name-display");
let lastNameInput = document.querySelector("#txt-last-name");

console.log(fullNameInput);
console.log(lastNameInput);

let inputLastName = document.querySelector("#txt-last-name");
let fullNameDisplay = document.querySelector("#full-name-display");

const showName = () =>{
  console.log(inputFirstName.value);
  console.log(inputLastName.value);
  // console.log(fullNameDisplay.innerHTML);
  fullNameDisplay.innerHTML = `${inputFirstName.value} ${inputLastName.value}`;
}

inputFirstName.addEventListener('keyup',showName)
inputLastName.addEventListener('keyup',showName)

