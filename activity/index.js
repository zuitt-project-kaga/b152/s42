let firstName = document.getElementById("txt-first-name");
let lastName = document.getElementById("txt-last-name");
let fullnameDisplayer = document.getElementById("full-name-display");
let button = document.getElementById("submit-btn");

const nameMerge = () => {
  fullnameDisplayer.innerHTML = `${firstName.value} ${lastName.value}`;
};

firstName.addEventListener("keyup", nameMerge);
lastName.addEventListener("keyup", nameMerge);

button.addEventListener("click", () => {
  firstName.value.length > 0 && lastName.value.length > 0
    ? (alert(`Thank you for registering ${firstName.value} ${lastName.value}`),
      (firstName.value = ""),
      (lastName.value = ""),
      (fullnameDisplayer.innerHTML = "")).then(
        (window.location.href = "https://zuitt.co")
      )
    : alert("Please input firstname and lastname!");
});
